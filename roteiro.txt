git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: Inicializa/cria um repositório

git status: Mostra o estado atual do repositório (Útil para verificar arquivos ou alterações ainda não rastreadas ou não commitadas)

git add arquivo/diretório: Passa o arquivo informado para Staging Area, tornando possível um futuro commit
git add --all = git add .: Passa todos os arquivos modificados para Staging Area

git commit -m “Primeiro commit”: Cria um "ponto na história" daquele arquivo
Graças aos commits, é possível navegar entre diversas versões do código
Extremamente importante sempre ser o mais claro possível nos commits para facilitar a navegação futuramente

-------------------------------------------------------

git log: Mostra o histórico de commits de um repositório
git log arquivo: Mostra o histórico de um arquivo específico
git reflog: Mostra um histórico local dos commits feitos pelo usuário (Útil para recuperar commits perdidos)

-------------------------------------------------------

git show: Mostra de forma visual as alterações realizadas no último commit
git show <commit>: Mostra de forma visual as alterações realizadas em um commit específico

-------------------------------------------------------

git diff: Mostra um comparativo das alterações (não rastreadas pelo git) e não retorna nada se não houver alterações
git diff <commit1> <commit2>: Mostra um comparativo das alterações dos commits informados 

-------------------------------------------------------

git reset --hard <commit>: Retorna o arquivo para o commit informado, apagando as alterações (commitadas ou não)

-------------------------------------------------------

git branch: Mostra as branches locais e destaca a atual (Pode ser utilizado para criar uma nova branch ao informar o nome desejado como parâmetro)
git branch -r: Mostra as branches remotas
git branch -a: Mostra todas as branches
git branch -d <branch_name>: Deleta a branch informada (Somente se a mesma já foi mesclada "merged")
git branch -D <branch_name>: Força a exclusão da branch informada, independente se a mesma foi mesclada ou não
git branch -m <nome_novo>: Altera o nome da branch atual
git branch -m <nome_antigo> <nome_novo>: Altera o nome da branch especificada pelo novo nome definido

-------------------------------------------------------

git checkout <branch_name>: Muda para branch informada 
git checkout -b <branch_name>: Cria uma nova branch e muda automaticamente para a mesma

-------------------------------------------------------

git merge <branch_name>: Mescla "merge" a branch informada com a atual

-------------------------------------------------------

git clone: Clona (faz o download) de um repositório remoto
git pull: Baixa as alterações do repositório remoto e altera no local
git push: Envia as alterações realizadas no repositório local para o remoto

-------------------------------------------------------

git remote -v: Mostra os nomes com as respectivas urls dos remotes (repositório remoto)
git remote add origin <url>: Cria um novo remote com o nome especificado na url informada 
git remote set-url origin <url>: Altera a url do remote informado para url especificada

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


